#!/bin/bash
sd=/scripts

bash $sd/mpstat.sh &
bash $sd/sar.sh &
bash $sd/uptime.sh &

while true; do
	echo 'Up and running...'
	sleep 60
done
