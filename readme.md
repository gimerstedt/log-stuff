# naive sysmon
both solutions run mpstat, sar and uptime every 5 seconds and output to /var/log/monitor.  
the logrotate unit file in the systemd directory can be used for both.

## docker
### build and start
```bash
cd docker && make
```

### stop
```bash
cd docker && make clean
```

## systemd
### install && start
```bash
cd systemd && make
```

### stop && uninstall
```bash
cd systemd && make uninstall
```
